import Numbers from "./Components/Numbers";
import {Component} from "react";

class App extends Component {

    render() {
        return (
            <div>
                <Numbers/>
            </div>
        );
    }
}

export default App;
