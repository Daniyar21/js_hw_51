import React from 'react';
import './Numbers.css'

class Numbers extends React.Component {
    state = {
        numbers: [0,0,0,0,0,0]
    }


    getArray = ()=>{
       const  getRandomNumber = (min, max) => {
            return Math.floor(Math.random() * (max - min + 1) + min)
        }
        const newNumbers = [];
        while (newNumbers.length < 5) {
            const number = getRandomNumber(5, 36);
            if (!newNumbers.includes(number)) {
                newNumbers.push(number);
            }
        }
        this.setState({ numbers: newNumbers.sort((a, b) => a > b ? 1 : -1)});
    }
    getNewNumbers = ()=>{
        this.getArray();
    }


    render() {
        return (
            <div>
                <div>
                    {this.state.numbers [0]}
                </div>
                <div>
                    {this.state.numbers [1]}
                </div>
                <div>
                    {this.state.numbers [2]}
                </div>
                <div>
                    {this.state.numbers [3]}
                </div>
                <div>
                    {this.state.numbers [4]}
                </div>



                <button onClick={this.getNewNumbers}>New Numbers</button>
            </div>

        );
    }
}

export default Numbers;